package TheProject2017;

public class SeanceTheatre extends Seance {
	SalleTheatre salleTheatre;
	int nbFauteuilsVendus;

	public SeanceTheatre(SalleTheatre salleTheatre, int jour, Heure horaire) {
		super(jour , horaire);
		this.salleTheatre = salleTheatre;
		
	}
public int getNbFauteuilsVendus () {
	return this.nbFauteuilsVendus;

}
	public int nbFauteuilsDispo() {
		return this.salleTheatre.nbFauteuils - this.nbFauteuilsVendus;
	}

	public void vendrePlacesFauteuil(int nbre) {
		this.nbFauteuilsVendus += nbre;
	}

	public int nbPlacesDispo() {
		return salleTheatre.capacite - totalVendu();
	}

	public int totalVendu() {
		return (this.nbFauteuilsVendus + this.nbPlacesVenduesTN);
	}

	public double tauxRemplissage() {
		return (totalVendu()*100)/salleTheatre.capacite;
	}
	public String toString() {
		return  "\n" + this.salleTheatre+"\n jour : " +this.jour + "\n a :  "+ this.horaire +"\n NbPlaceVendues : " + this.totalVendu()+"\n";		
	}
	public SalleTheatre getSalle() {
		return salleTheatre;
	}

	@Override
	public int compareTo(Seance arg0) {
		if (this.getJour() != arg0.getJour())
			return this.getJour() - arg0.getJour();
		else
			if(this.getHoraire().compareTo(arg0.getHoraire())!=0)
			return this.getHoraire().compareTo(arg0.getHoraire());
			else
			return this.salleTheatre.getNomSalle().compareTo(((SeanceTheatre)arg0).salleTheatre.getNomSalle());
	}
	
}
