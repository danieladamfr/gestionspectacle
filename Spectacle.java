package TheProject2017;

public abstract class Spectacle {
	String titre;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((interpretes == null) ? 0 : interpretes.hashCode());
		result = prime * result + ((titre == null) ? 0 : titre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Spectacle other = (Spectacle) obj;
		if (interpretes == null) {
			if (other.interpretes != null)
				return false;
		} else if (!interpretes.equals(other.interpretes))
			return false;
		if (titre == null) {
			if (other.titre != null)
				return false;
		} else if (!titre.equals(other.titre))
			return false;
		return true;
	}

	String interpretes;

	public String getTitre() {
		return this.titre;
	}

	public String getInterpretes() {
		return this.interpretes;
	}
	
	public Spectacle(String titre, String interpretes) {
		this.titre = titre;
		this.interpretes = interpretes;
	}

	// @Override
	public String toString() {
		return "Titre du film : " + this.titre + "\n Interpretes : " + this.interpretes;
	}
}
