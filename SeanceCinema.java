package TheProject2017;

public class SeanceCinema extends Seance implements Comparable<Seance>{
	Salle salle;
	int nbPlacesVenduesTR;

	public SeanceCinema(Salle salle, int jour, Heure horaire) {
		super(jour,horaire);
		this.salle = salle;
		
	}

	public void vendrePlacesTR(int nbre) {
		this.nbPlacesVenduesTR += nbre ;
	}

	public int nbPlacesDispo() {
		return salle.capacite - totalVendu();
	}

	public int totalVendu() {
		return this.nbPlacesVenduesTN + this.nbPlacesVenduesTR;
	}

	public double tauxRemplissage() {
		return (this.totalVendu()*100)/salle.capacite;
	}



	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeanceCinema other = (SeanceCinema) obj;
		if (salle == null) {
			if (other.salle != null)
				return false;
		} else if (!salle.equals(other.salle))
			return false;
		return true;
	}

	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public int getNbPlacesVenduesTR() {
		return nbPlacesVenduesTR;
	}

	public void setNbPlacesVenduesTR(int nbPlacesVenduesTR) {
		this.nbPlacesVenduesTR = nbPlacesVenduesTR;
	}

	public String toString() {
		return  this.salle+"    jour : " +this.jour + "    a :  "+ this.horaire +"    NbPlaceVendues : " + this.nbPlacesVenduesTR+ " \n \n";		
	}

	@Override
	public int compareTo(Seance arg0) {
		if (this.getJour() != arg0.getJour())
			return this.getJour() - arg0.getJour();
		else
			if(this.getHoraire().compareTo(arg0.getHoraire())!=0)
			return this.getHoraire().compareTo(arg0.getHoraire());
			else
			return this.salle.getNomSalle().compareTo(((SeanceCinema)arg0).salle.getNomSalle());
	}



}
