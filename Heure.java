package TheProject2017;

public class Heure implements Comparable<Heure>{
	private int heures;
	private int minutes;

	public Heure(int heures, int minutes) {
		this.heures = heures;
		this.minutes = minutes;
	}

	public int getHeures() {
		return heures;
	}

	public int getMinutes() {
		return minutes;
	}

	public String toString() {
		return this.heures + ":" + this.minutes;
	}

	public int compareTo1(Heure h) {
		if (this.heures < h.heures) 
			return -1;
		else if(this.heures > h.heures) 
			return 1;
		else if (this.minutes < h.minutes) 
			return -1;
		else if(this.minutes > h.minutes) 
			return 1;

		return 0;
	}

	@Override
	public int compareTo(Heure arg0) {
		// negative si qui appelle est plus grande

		if (heures < arg0.heures || (heures == arg0.heures && minutes < arg0.minutes))
			return -1;
		if (heures > arg0.heures || (heures == arg0.heures && minutes > arg0.minutes))
			return 1;
		return 0;
	}

	
	public boolean equals1(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Heure other = (Heure) obj;
		if (heures != other.heures)
			return false;
		if (minutes != other.minutes)
			return false;
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Heure other = (Heure) obj;
		if (heures != other.heures)
			return false;
		if (minutes != other.minutes)
			return false;
		return true;
	}
	

	public void setHeures(int heures) {
		this.heures = heures;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}
}